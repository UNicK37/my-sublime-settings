# README #

This repo stores all settings with packages I used for my Sublime Text 3.

### What is this repository for? ###

* My own sublime settings.
* My own sublime packages sets.
* My own sublime theme settings.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* For Windows users:
  please clone this repo to: %AppData%\Sublime Text 3\Packages\User,
  and Sublime will do all required setups for you.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact